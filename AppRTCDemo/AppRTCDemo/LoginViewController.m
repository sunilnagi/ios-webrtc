//
//  LoginViewController.m
//  AppRTCDemo
//
//  Created by Pawan Saini on 05/07/14.
//  Copyright (c) 2014 Google. All rights reserved.
//

#import "LoginViewController.h"
#import "ContactsViewController.h"

#import <AVFoundation/AVFoundation.h>
#import "APPRTCConnectionManager.h"
#import "RTCEAGLVideoView.h"

@interface LoginViewController ()<APPRTCConnectionManagerDelegate, APPRTCLogger, RTCEAGLVideoViewDelegate>

@end

@implementation LoginViewController
{
    APPRTCConnectionManager* _connectionManager;
}

- (instancetype)initWithNibName:(NSString*)nibName
                         bundle:(NSBundle*)bundle {
    if (self = [super initWithNibName:nibName bundle:bundle]) {
        _connectionManager =
        [[APPRTCConnectionManager alloc] initWithDelegate:self
                                                   logger:self];
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
     self.busyView.hidden=TRUE;
    [super viewWillAppear:YES];
}
- (void)viewDidLoad
{
    self.navigationItem.title=@"App RTC";
   
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)btnLogin:(id)sender
{
    NSString *userid = self.txtlogin.text;
    if ([userid length] == 0) {
        return;
    }

    if(self.txtlogin.text.length>0)
    {
        [self.txtlogin resignFirstResponder];
        NSUserDefaults *user = [[NSUserDefaults alloc] init];
        [user setObject:self.txtlogin.text forKey:@"UserID"];
        [user synchronize];
        
        self.busyView.hidden=FALSE;
        
        //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserID"];
        //[[NSUserDefaults standardUserDefaults]synchronize ];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]);
        
        NSString* url =
        [NSString stringWithFormat:@"https://yabbrtc.appspot.com/?u=%@", self.txtlogin.text];
        NSLog(@"%@",url);
        [_connectionManager connectToRoomWithURL:[NSURL URLWithString:url]];
        
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter phone no." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    

    
    
}

- (void)applicationWillResignActive:(UIApplication*)application {
    //[self logMessage:@"Application lost focus, connection broken."];
    //[self disconnect];
}

#pragma mark - APPRTCConnectionManagerDelegate

- (void)connectionManager:(APPRTCConnectionManager*)manager
didReceiveLocalVideoTrack:(RTCVideoTrack*)localVideoTrack {
    //self.localVideoView.hidden = NO;
    //self.localVideoView.videoTrack = localVideoTrack;
}

- (void)connectionManager:(APPRTCConnectionManager*)manager
didReceiveRemoteVideoTrack:(RTCVideoTrack*)remoteVideoTrack {
    //self.remoteVideoView.videoTrack = remoteVideoTrack;
}

- (void)connectionManagerDidReceiveHangup:(APPRTCConnectionManager*)manager {
    //[self showAlertWithMessage:@"Remote hung up."];
   // [self disconnect];
}

- (void)connectionManager:(APPRTCConnectionManager*)manager
      didErrorWithMessage:(NSString*)message {
    [self showAlertWithMessage:message];
    //[self disconnect];
}

#pragma mark - APPRTCLogger

- (void)logMessage:(NSString*)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString* output =
        [NSString stringWithFormat:@"\n%@", message];
        NSLog(@"%@",output);
        
    });
}

#pragma mark - RTCEAGLVideoViewDelegate


#pragma mark - UITextFieldDelegate

#pragma mark - Private

//- (void)disconnect {
//    [self resetUI];
//    [self showAlertWithMessage:@"Login Remote hung up."];
//    [_connectionManager disconnect];
//}

- (void)showAlertWithMessage:(NSString*)message {
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void)resetUI {
    
}

- (void)setupCaptureSession {
    
  
    /*
     OLD CODE
     self.blackView.hidden = NO;
     self.remoteVideoView =
     [[RTCEAGLVideoView alloc] initWithFrame:self.blackView.bounds];
     self.remoteVideoView.delegate = self;
     self.remoteVideoView.transform = CGAffineTransformMakeScale(-1, 1);
     [self.blackView addSubview:self.remoteVideoView];
     
     self.localVideoView =
     [[RTCEAGLVideoView alloc] initWithFrame:self.blackView.bounds];
     self.localVideoView.delegate = self;
     [self.blackView addSubview:self.localVideoView];
     [self updateVideoViewLayout];
     */
}



@end
