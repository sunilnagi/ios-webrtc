/*
 * libjingle
 * Copyright 2013, Google Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. The name of the author may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "APPRTCAppDelegate.h"

#import "APPRTCViewController.h"
#import "RTCPeerConnectionFactory.h"
#import "LoginViewController.h"
#import "ContactsViewController.h"
@implementation APPRTCAppDelegate {
  UIWindow* _window;
    
    
}

#pragma mark - UIApplicationDelegate methods
@synthesize navigationController=_navigationController;
@synthesize IsSameUser=_IsSameUser;
@synthesize initiator=_initiator;
@synthesize callConnectedWithUserID=_callConnectedWithUserID;
@synthesize you=_you;
- (BOOL)application:(UIApplication*)application
    didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
  [RTCPeerConnectionFactory initializeSSL];
  _window =  [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
//  APPRTCViewController* viewController =
//      [[APPRTCViewController alloc] initWithNibName:@"APPRTCViewController"
//                                             bundle:nil];
    
    LoginViewController* viewController =
    [[LoginViewController alloc] initWithNibName:@"LoginViewController"
                                           bundle:nil];
    
    
    self.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
    
  _window.rootViewController = self.navigationController;
  [_window makeKeyAndVisible];
  return YES;
}

- (void)applicationWillResignActive:(UIApplication*)application {
  //ß®[[self appRTCViewController] applicationWillResignActive:application];
}


- (void)applicationWillTerminate:(UIApplication*)application {
  [RTCPeerConnectionFactory deinitializeSSL];
}

#pragma mark - Private

- (LoginViewController*)appRTCViewController {
  return (LoginViewController*)_window.rootViewController;
}

@end
