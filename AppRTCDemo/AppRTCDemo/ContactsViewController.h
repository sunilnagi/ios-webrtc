//
//  ContactsViewController.h
//  AppRTCDemo
//
//  Created by Pawan Saini on 05/07/14.
//  Copyright (c) 2014 Google. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "APPRTCAppDelegate.h"
@protocol contactsDelegate<NSObject>
@optional
- (void)MakeCall;
@end
@interface ContactsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    APPRTCAppDelegate *mainDelegate;
    
    id<contactsDelegate> smalldelegate;
}
@property(nonatomic,retain) IBOutlet UITableView *tblView;
@property (nonatomic,retain) id  smalldelegate;
@end
