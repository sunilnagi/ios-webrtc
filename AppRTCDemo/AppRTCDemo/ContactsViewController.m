//
//  ContactsViewController.m
//  AppRTCDemo
//
//  Created by Swati Nagi on 05/07/14.
//  Copyright (c) 2014 Google. All rights reserved.
//

#import "ContactsViewController.h"

#import "APPRTCViewController.h"
@interface ContactsViewController ()

@end

@implementation ContactsViewController
@synthesize tblView;
@synthesize smalldelegate;
NSMutableArray *arrContacts;
NSMutableArray *arrJsonData;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
   
    arrContacts=[[NSMutableArray alloc]init];
    self.navigationItem.title=@"Contacts";
    
    [self loadData];

    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadData)];
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)loadData
{
    HUD=[[MBProgressHUD alloc] initWithView:self.view];
    // HUD.graceTime=0.5;
    HUD.delegate=self;
    // HUD.dimBackground=TRUE;
    [self.view addSubview:HUD];
    [HUD showWhileExecuting:@selector(fetchContactsFromServer) onTarget:self withObject:nil animated:TRUE];
}
-(void)fetchContactsFromServer
{
    //self.tblView = nil;
    [arrContacts removeAllObjects];
    NSMutableData *receivedData;
    NSString *responseString=@"";
   
    
    //http://182.71.17.18/iPhone-Assets/testJson.js
    receivedData =[[NSMutableData alloc] initWithContentsOfURL:[NSURL URLWithString:@"https://yabbrtc.appspot.com/contacts"]];
    
    if (receivedData != nil)
    {
        responseString = [[NSString alloc] initWithBytes: [receivedData mutableBytes]
                                                  length:[receivedData length]
                                                encoding:NSUTF8StringEncoding];
        
    }
   // NSLog(responseString);
   
  
    //NSData* constraintsData =
    //[mediaConstraints dataUsingEncoding:NSUTF8StringEncoding];
   // NSDictionary* constraintsJSON = [self parseJSONData:constraintsData];
//
    
    NSMutableDictionary *dicData= [self parseJSONData:receivedData];
    
    arrJsonData=[dicData objectForKey:@"contacts"];
   
    //remove login user's userID
    for(int i=0;i<[arrJsonData count];i++)
    {
        NSMutableDictionary *dicphone=[arrJsonData objectAtIndex:i];
        
        //NSLog(@"%@",dicData);
        //error
        if (![[dicphone objectForKey:@"contact"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]])
        {
            [arrContacts addObject:dicphone];
            
        }
    }
    
    [tblView reloadData];
    
}

- (NSMutableDictionary*)parseJSONData:(NSData*)data {
    NSError* error = nil;
    NSMutableDictionary* json =
    [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    NSAssert(!error, @"Unable to parse.  %@", error.localizedDescription);
    return json;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrContacts count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dicphone=[arrContacts objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    }
    
    NSString *cellvalue=@"";
    if ([[dicphone objectForKey:@"contact"] isEqual:[NSNull null]])
    {
        cellvalue=@"";
    }
    else{
        cellvalue=[dicphone objectForKey:@"contact"];
    }
     //cell.textLabel.text=[arrContacts objectAtIndex:indexPath.row];
    cell.textLabel.text=cellvalue;
    
    //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    mainDelegate=(APPRTCAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    mainDelegate.initiator=TRUE;
    mainDelegate.you=[NSString stringWithFormat:@"%@",[[arrContacts objectAtIndex:indexPath.row] objectForKey:@"contact"]];
    
    //NSLog(@"%@",self.smalldelegate);
    if ([self.smalldelegate respondsToSelector:@selector(MakeCall)]) {
        [self.smalldelegate MakeCall];
    }
}

@end
