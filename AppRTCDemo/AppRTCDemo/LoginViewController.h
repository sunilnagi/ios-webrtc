//
//  LoginViewController.h
//  AppRTCDemo
//
//  Created by Pawan Saini on 05/07/14.
//  Copyright (c) 2014 Google. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property(weak,nonatomic) IBOutlet UITextField *txtlogin;
@property(weak,nonatomic) IBOutlet UIView *busyView;
- (void)applicationWillResignActive:(UIApplication*)application;
@end
